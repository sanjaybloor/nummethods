#include <iostream>
#include <random>
#include <ctime>
#include <cmath>
#include "MCint.h"
using namespace std;

double sampler(double x, double a, double b) {
  // Samples from distrubition W(x)=Ax+B;
  double B = 0.98;
  double A = 0.5-B;
  double out = (-B+sqrt(B*B+2*A*x))/A;
  return out;
}

double pdf(double x, double a, double b) {
  //
  double B = 0.98;
  double A = 0.5-B;
  double out = (A*x + B);
  return out;
}

double integrand(double x, double a, double b) {
  // Integrand erf(2)
  return 2*exp(-x*x)/sqrt(M_PI);
}

int main() {

  int N = 128;  // Number of initial sampling points
  double a = 0; // Start point of integrand
  double b = 2; // End point of integrand

  cout << "Starting with " << N << " initial points." << endl;

  double I = MCint(integrand, sampler, pdf, a, b, 1E-6, N);
  
  cout << "Area under curve = " << I << endl;
  return 0;
}
