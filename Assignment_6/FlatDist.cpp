#include <iostream>
#include <random>
#include <ctime>
#include <cmath>
#include "MCint.h"
using namespace std;

double sampler(double x, double a, double b) {
  // Does not alter x
  return x;
}

double pdf(double x, double a, double b) {
  // Distrubition W(x), simple flat distribution between a and b.
  double out = 1.0/(b-a);
  return out;
}

double integrand(double x, double a, double b) {
  // Integrand erf(2)
  return 2*exp(-(b-a)*(b-a)*x*x)/sqrt(M_PI);
}

int main() {

  int N = 128;  // Number of initial sampling points
  double a = 0; // Start point of integrand
  double b = 2; // End point of integrand

  cout << "Starting with " << N << " initial points." << endl;

  double I = MCint(integrand, sampler, pdf, a, b, 1E-6, N);
  
  cout << "Area under curve = " << I << endl;
  return 0;
}
