#include <iostream>
#include <random>
#include <ctime>
using namespace std;

// Performs Monte Carlo integration for the specified integrand [f(x)] 
// and using the specified sampling distribution [W(x)]. Samples to a relative accuracy 
// of epsilon with initial number of points N. Rescales the samples from a to b.

double MCint(double (*integrand)(double, double, double), double (*distribution)(double, double, double), double (*pdf)(double, double, double), double a, double b, double epsilon, int N) {

  double t = time(0); // Uses clock time as seed.
  mt19937 gen(t);     // Random number generator. (Mersenne Twister) 
  uniform_real_distribution<double> dis(0, 1); // Specifies range [0,1]
  double x;           // Randomly distributed variable
  double x_prime;     // Adjusted by sampler

  double Inew = 0;    // Latest etimate of integral
  double Iold = 0;    // Previous estimate of integral
  double R;           // Fractional ratio of integrals for convergence test
  int j=0;            // Counter 

  while (j > -1) {

    // Set estimate of I to the old one
    Iold = Inew;
  
    // Reset new estimate of I
    Inew = 0;

    // Sampling
    for (int i=0; i<N; i++) { 
      x =  dis(gen);  // Generate random number 
      x_prime = distribution(x, a, b); // Obtains new value of x from user specified distribution
      Inew = Inew + integrand(x_prime, a, b)/pdf(x_prime, a, b);  
    }
 
    Inew = Inew/N; // Normalise

    R = abs(Inew-Iold)/Inew;  

    cout << Inew << " " << Iold << " " << R << endl;

    if (R < epsilon) {
      // Done! Return value of I.
      return 0.5*(Inew+Iold);
      return j;
    }

    else if (R > epsilon) {
      // Repeat. Double N. Add iteration. 
      N = 2*N;
      j++;
      cout << j << endl;
    }

  }
  return 0;
}

