double MCint(double (*integrand)(double, double, double), double (*distribution)(double, double, double), double (*pdf)(double, double, double), double a, double b, double epsilon, int N);
