#include <iostream>
#include "RootFinder.h"
#include <fstream>
#include <string>
#include <random>
#include <ctime>
using namespace std;


double f(double xin) {

  // Lazy implementation. Reads in data points (Yep, every function call :~)) and computes f(x) for cubic spline

  int n = 11; // 11 lines
  double x[n], y[n], yPP[n], t1, t2, t3, A, B, C, D, yout;
  ifstream infile; // Read it all in
  infile.open("Output_specified.dat");
  int i=0;
  while (infile >> t1 >> t2 >> t3) {
    x[i] = t1;
    y[i] = t2;
    yPP[i] = t3;
    ++i;
  }
  infile.close();

  int counter = 0;
  // Set value of counter to determine which two points need to be used to interpolate
  for (int i=0; i<n; i++) {
    if (xin > x[i]) {
      counter++;
    } 
  }   
  //cout << xin << " " << counter << endl;
  double var = x[counter]-x[counter-1]; // Spacing variable
  A = (x[counter]-xin)/var;
  B = 1-A;
  C = (A*A*A-A)*var*var/6;
  D = (B*B*B-B)*var*var/6;
  yout = A*y[counter-1] + B*y[counter] + C*yPP[counter-1] + D*yPP[counter];
  return yout;
}

int main() {

  // "Knowledge of the geometry of the problem" tells me there's 2 roots. Use a 
  // little bit of logic and a bit of randomness to find a negative value for f(x). 

 
  // Find a point between -2.1 and 3.8 where f(x) is negative
  double a_1 = -2.1, b_2 = 3.8;
  double a_2, b_1;
  
  /*
  double t = time(0);
  mt19937 gen(t);      // Random number generator. (Mersenne Twister) 
  uniform_real_distribution<double> dis(a_1, b_2); // Specifies range [0,1]

  a_2 = 0;
  while (f(a_2) > 0) {  
    a_2 = dis(gen);    
    cout << a_2 << " " << f(a_2) << endl;
  }
  cout << a_2 << endl;
  b_1 = a_2;
  */ 
  a_2 = b_1 = 2.26;
  double delta = 1E-8;
  double root1 = 0, root2 = 0;

  double starttime = clock();
  // Bisection
  root1 = bisector(f, a_1, b_1, delta, 100);
  root2 = bisector(f, a_2, b_2, delta, 100);
  double endtime = clock();
  double timetaken = (endtime - starttime)/CLOCKS_PER_SEC;
  cout << "Bisection: time taken = " << timetaken << "s." << endl;
  cout << root1 << " " << root2 << endl;

  starttime = clock();
  // Brent
  root1 = brent(f, a_1, b_1, delta, 100);
  root2 = brent(f, a_2, b_2, delta, 100);
  endtime = clock();
  timetaken = (endtime - starttime)/CLOCKS_PER_SEC;
  cout << "Brent's method: time taken = " << timetaken << "s." << endl;
  cout << root1 << " " << root2 << endl;

  return 0;
}
