double bisector(double (*function)(double), double a, double b, double delta, int max_it);
double brent(double (*function)(double), double a, double b, double delta, int max_it);

