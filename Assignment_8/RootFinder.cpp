#include <iostream>
#include <cmath>
using namespace std;

// Find better way of throwing errors for bad brackets.

double bisector(double (*function)(double), double a, double b, double delta, int max_it) {
  delta = abs(delta); // Make the tolerance positives
  // Finds the root of a function f(x), by bisection, between specified a and b... 
  // ...Assuming there is one. Iterates to an accuracy delta.
 
  cout << "Initial brackets are: " << a << " and " << b << endl;
  
  // Firstly, check to see if the bracketing will include a root.
  double fa = function(a);
  double fb = function(b);
  if (fa*fb > 0) {
    throw "Use better brackets, chump. I'm done with you.";
  }
  else if (fa*fb == 0) {
    throw "You've just given me a root. What are you playing at?";
  }
  else if (fa*fb < 0) {
    cout << "Alright sailor, your brackets are okay. I'll get going." << endl;
  }

  double midpoint = (a+b)/2, fmid=function(midpoint); int counter;

  for (int j=0; j<max_it; j++) {
    if (abs(a-b) < delta) {
      cout << "This took " << counter << " iteration(s)." << endl;      
      return midpoint;
    }
    counter = j+1;
    if (fmid*fa > 0) {
      // Replace a with midpoint if sign is the same
      a = midpoint;
    }
    else {
      // Otherwise, replace b
      b = midpoint;
    }
    midpoint = (a+b)/2;
    fmid = function(midpoint);
  }

  // Return value of root once if max_it is reached.
  cout << "Algorithm reached maximum number of iterations." << endl;
  return midpoint;
}

double brent(double (*function)(double), double a, double c, double delta, int max_it) {
  // Finds the root of a function f(x), using Brent's method, between specified a and b... 
  // ...Assuming there is one. Iterates to an accuracy delta or until max_it iterations.
  delta = abs(delta); // Make the tolerance positive

  cout << "Initial brackets are: " << a << " and " << c << endl;  
  cout << "Initial values are: " << function(a) << " and " << function(c) << endl;

  // Firstly, check to see if the bracketing will include a root.
  double fa = function(a);
  double fc = function(c);
  if (fa*fc > 0) {
    throw "Use better brackets, chump. I'm done with you.";
  }
  else if (fa*fc == 0) {
    throw "You've just given me a root. What are you playing at?";
  }
  else if (fa*fc < 0) {
    cout << "Alright sailor, your brackets are okay. I'll get going." << endl;
  }

  cout << "Initial f(x) values are: " << fa << " and " << fc << endl;

  double b, fb; // b and f(b)
  double x, fx; // Temp (computed) value of x, f(x)
  double P, Q, R, S, T, d, temp;
  int counter=0;

  // If c is closer than a to the root, swap 'em.
  if (abs(fa) > abs(fc)) {
    swap(a, c);
    swap(fa, fc);
  } 

  // Set b = a. 
  b = a; 
  fb = fa;

  int flagint = 1; // Flag to determine whether or not interpolation was used last.
  // 0 = interpolation (quadratic or linear), 1 = bisection. 

  for (int j=0; j<max_it; j++) { 
    counter = j+1; 

    if (abs(fb) < delta || fb == 0) { // Convergence criteria.
      cout << "This took " << counter << " iteration(s)." << endl;      
      return b;
    }

    S = fb/fa;         
    // Quadratic interpolation        
    if (fa != fb && fa != fc) {
      R = fb/fc; 
      T = fa/fc;
      P = S*(T*(R-T)*(c-b)-(1-R)*(b-a));
      Q = (T-1)*(R-1)*(S-1);
      x = b + P/Q;     
    }
    // Secant method
    else { 
      P = fb*(c-b);
      Q = (fb-fc);
      x = b + P/Q;  
    }
    x = b + P/Q;  

    // If any of the conditions are met -> use bisection
    if ( ( flagint == 1 && abs(P/Q) >= 0.5*abs(b-c) ) || 
         ( flagint == 1 && abs(b-c) < 0.5*delta ) ||
         ( flagint == 0 && abs(P/Q) >= 0.5*abs(c-temp) ) || 
         ( flagint == 0 && abs(c-temp) < 0.5*delta ) ||
         ( abs(P/Q) >= 0.75*abs(c-b) ) ) {
      x = 0.5*(a+b);
      flagint = 1; // Set flag to indicate bisection
    }
    // Otherwise set flag to indicate interpolation/secant
    else {
      flagint = 0;
    }

    fx = function(x);

    // For |P\Q|_{j-2}
    temp = c;
    
    if (fx*fa < 0) { // Estimate is on opposite side to before
      a = c; fa = fc;
      c = b; fc = fb;
      b = x; fb = fx;
    }
    else { // Estimate is on same side
      a = b; fa = fb;
      b = x; fb = fx;
    }

    // If old guess was better, flip 'em, and secant. 

    if (abs(fb) > abs (fc)) {
      swap(b, c);
      swap(fb, fc);
      a = b; fa = fb;
    }

  }
  
  // Return value of root once if max_it is reached.
  cout << "Algorithm reached maximum number of iterations." << endl;
  return b;
}

