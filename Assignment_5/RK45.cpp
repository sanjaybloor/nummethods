#include <iostream>
#include <cmath>
#include "RK45function_2.h"
#include "RK45function.h"
using namespace std;

// Program using a Runge Kutta 4-5 method with adaptive stepsize via step doubling. 
// Only for initial valued problems of N coupled ODEs. Requires external file 
// function.cpp and header file function.h containing the appropriate definitions for
// the ODEs, the boundary conditions, initial conditions, etc. See the README for
// relevant information. 

double RK4_it(double& x, double y[], double h, int N){  
  // Performs one iteration of RK4. 

  // x -> dependent variable
  // y[] -> solution vector, y=y(x).
  // h -> step size.
  // N -> number of coupled equations: y=y[N].

  double k1[N], k2[N], k3[N], k4[N], ktemp[N], ktempb[N]; 
  // 2 temp variables needed so as to not use the adjusted values in the same iteration.
  // ... this was a bug for way longer than I care to admit.

  for (int i=0; i<N; i++) {
    k1[i] = h*f(x, y, i);
    ktemp[i] = y[i] + k1[i]/2;
  }
  for (int i=0; i<N; i++) {
    k2[i] = h*f(x+h/2, ktemp, i);
    ktempb[i] = y[i] + k2[i]/2;
  }
  for (int i=0; i<N; i++) {
    k3[i] = h*f(x+h/2, ktempb, i);
    ktemp[i] = y[i] + k3[i];
  }
  for (int i=0; i<N; i++) {
    k4[i] = h*f(x+h, ktemp, i);
  }
  x = x+h;
  for (int i=0; i<N; i++) {
    y[i] = y[i] + (k1[i] + 2*k2[i] + 2*k3[i] + k4[i])/6;
  }
}

int main() {

  // Start point (a) and end point (b) imported from function.cpp.
  // Solution vector y[N] (for N couples ODEs) also imported, with 
  // initial conditions. Here, y=y(x). 

  // Initialise the dependent variable at the initial boundary.
  double x = a; 

  // Create duplicates of x and y for the estimates.
  double x_1 = a, y_1[N], x_2 = a, y_2[N]; 

  // Insert boundary conditions to estimates of y.
  for (int i=0; i<N; i++) {  
      y_1[i] = y[i];
      y_2[i] = y[i];
  }

  // Variables for error checking: vector of errors, largest error, safety factor & error.
  double delta[N], del=0, S=0.9, eps;  
  
  if (errorflag == 0) {
    eps = 1E-4;
  }

  double ymax=0; // For relative error, store the largest value of y_2

  // Try initial step size of 1% of the domain
  double h=(b-a)/100; 

  int flag = 0; // Flag telling whether or not to print.

  // Print initial conditions
  cout << x << " ";
  for (int i=0; i<N; i++) {
    cout << y[i] << " ";
  }
  cout << endl; 

  while (x < b) {     // Perform RK4-5 routine until we reach the end point, x=b.

    if (x+h > b) { // Set condition so last iteration is at b exactly. 
      h = b-x;
    }
    
    // Take 2 step sizes of h/2
    RK4_it(x_2, y_2, h/2, N); RK4_it(x_2, y_2, h/2, N);

    // Take 1 normal step of h
    RK4_it(x_1, y_1, h, N); 

    // At this point, we have estimates y_2{n+1} and y_1{n+1} for y{n+1}.
    // Compute vector of errors "delta", then find largest value, "del".

    for (int i=0; i<N; i++) {
      delta[i] = abs(y_2[i] - y_1[i]); 
      if (delta[i] > del) {
        del = delta[i];
      }        
    }

    // If error is relative, change eps now
    if (errorflag == 1) {
      for (int i=0; i<N; i++) {
        if (y_2[i] > ymax) {
          ymax = y_2[i];
        }
      }
      eps = 10E-6*ymax;
      ymax = 0; // Reset ymax
    }

    // Is the step size suitable? 

    // Yes! Permit the step h. Set y to the (greater accuracy) y_2.
    if (del < eps) { 
      x = x + h;
      for (int i=0; i<N; i++) {
        y[i] = y_2[i];
      }
      flag = 1; // Set flag so the new solutions get printed out.
    }

    // No! Do not permit the step h. Reset x_1/2 and y_1/2 to the previous x and y.
    else if (del >= eps) { 
      x_1 = x; x_2 = x;
      for (int i=0; i<N; i++) {
        y_1[i] = y[i];
        y_2[i] = y[i];
      }
    }

    h = h*pow(eps/del, 0.2); // Recompute step length

    if (flag == 1) { // Print when the step has been taken.
      cout << x << " ";
      for (int i=0; i<N; i++) {
        cout << y[i] << " ";
      }
      cout << endl;
      flag = 0; // Reset flag.
    }

    del = 0; // Reset the value of the error, delta

  }	

  return 0;
}


