#include <iostream>
#include <cmath>
using namespace std;

extern const int N = 2;    // Number of ODEs
double a = 0;              // Start point
double b = 10;             // End point
double y[N] = {1.5, 1.5};  // Initial conditions
int errorflag = 0;      // If '0' use fixed error, if '1' use relative

double f(double x, double y[], int j) { // x = position, y[](x) = solution vector, j iterates 0:N to select the correct function
  if (j == 0) {
    return -y[0]*y[0] - y[1];
  }
  else if (j == 1) {
    return 5*y[0] - y[1];
  }
}


