#include <iostream>
#include <cmath>
#include "RK45function_2_s.h"
using namespace std;

double RK4_s_it(double& x, double& y, double h){  
  // Performs one iteration of the adaptive Simpson's rule 

  // x -> dependent variable
  // h -> step size.

  y = y+h*(f(x)+4*f(x+h/2)+f(x+h))/6;
  x = x+h;
}

int main() {

  // Start point (a) and end point (b) imported from function.cpp.
  // Solution y (1d) also imported, y=y(x). 

  // Initialise the dependent variable at the initial boundary.
  double x = a; 

  // Create duplicates of x and y for the estimates.
  double x_1 = a, y_1=y, x_2 = a, y_2=y; 

  // Variables for error checking: error, safety factor & specified error.
  double del=0, S=0.9, eps;  
  
  if (errorflag == 0) {
    eps = 1E-4;
  }

  // Try initial step size of 1% of the domain
  double h=(b-a)/100; 

  int flag = 0; // Flag telling whether or not to print.

  // Print initial conditions
  cout << x << " " << y << endl;

  while (x < b) {     // Perform RK4-5 routine until we reach the end point, x=b.

    if (x+h > b) { // Set condition so last iteration is at b exactly. 
      h = b-x;
    }
    
    // Take 2 step sizes of h/2
    RK4_s_it(x_2, y_2, h/2); RK4_s_it(x_2, y_2, h/2);

    // Take 1 normal step of h
    RK4_s_it(x_1, y_1, h); 

    // At this point, we have estimates y_2{n+1} and y_1{n+1} for y{n+1}.
    del = abs(y_2 - y_1); 

    // If error is relative, change eps now
    if (errorflag == 1) {
      eps = 10E-6*y_2;
    }

    // Is the step size suitable? 

    // Yes! Permit the step h. Set y to the (greater accuracy) y_2.
    if (del < eps) { 
      x = x + h;
      y = y_2;
      flag = 1; // Set flag so the new solutions get printed out.
    }

    // No! Do not permit the step h. Reset x_1/2 and y_1/2 to the previous x and y.
    else if (del >= eps) { 
      x_1 = x; x_2 = x;
        y_1 = y;
        y_2 = y;
    }

    h = h*pow(eps/del, 0.2); // Recompute step length

    if (flag == 1) { // Print when the step has been taken.
      cout << x << " " << y << endl;
      flag = 0; // Reset flag.
    }

    del = 0; // Reset the value of the error, delta

  }	

  return 0;
}


