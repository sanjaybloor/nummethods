#include <iostream>
#include <cmath>
using namespace std;

double a = 0;             // Start point
double b = 2;             // End point
double y = 0;             // Initial conditions 
int errorflag = 1;        // If '0' use fixed error, if '1' use relative

double coeff = 2/sqrt(M_PI);

double f(double x) { // x = position
  return coeff*exp(-x*x);
}
