Files contained within this directory are:

 ------------------
|.cpp and .h files |
 ------------------

RK45.cpp 
To be used with the header files RK45function.cpp/h and RK45function_2.cpp/h. Reads in initial conditions and
functional forms from the associated/specified file, discussed below. Performs Runge-Kutta 4-5 evaluations to 
a user-specified machine accuracy, eps. Returns the values in the terminal. 
For question 1, type make to compile.
For question 2, type make -f Makefile2 to compile. 

RK45_Adaptive_Simpsons.cpp
Optimised 1-D form of RK45.cpp. Compile with make -f Makefile3. Uses the function files RK45function_2_s.cpp/h.

Function files
These require the user to specify the following: 

extern const int N;                       // Number of ODEs
double a;                                 // Start point
double b;                                 // End point
double y[N] = {0};                        // Initial conditions, y(a), of array size N. Defaults to 0.
int errorflag = 1;                        // If '0' use fixed error, if '1' use relative
function f(x, y[], j);                    // Flag j corresponds to the function needed for each ODE. 
                                          // Otherwise, this should be instructive. 

Otherwise the RK45 solvers WILL NOT WORK. These obviously must be reflected correctly in the header files.
