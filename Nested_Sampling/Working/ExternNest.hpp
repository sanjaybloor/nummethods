#include <armadillo>
double Nest(double (*likelihood)(const arma::vec& v), arma::vec (*prior)(int), int ns, int steps, const int N, double alpha);
arma::vec matmean(const arma::mat& x);
arma::mat elli_sample(const arma::mat& s);



