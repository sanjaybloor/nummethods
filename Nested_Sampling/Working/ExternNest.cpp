#include <iostream>
#include <cmath>
#include <random>
#include <ctime>
#include <armadillo>

arma::vec matmean(const arma::mat& x) 
{
  // Returns mean of each row of input matrix x
  int ns = x.n_rows;
  int nd = x.n_cols;
  arma::vec mu(nd);
  for (int i=0; i<nd; i++) 
  {
    for (int j=0; j<ns; j++) 
    {
      mu(i) = mu(i) + x(j,i);
    }  
  }
  mu = mu/ns;
  return mu;
}

/* INCOMPLETE CODE COMMENTED OUT: ATTEMPT AT MULTI-NEST LIKE SCENARIO -> DIDN'T REALLY WORK. 
 
double factorial(int x)  
{
  // Returns the factorial of a number
  int y=1; 
  for(int i=1; i<=x; i=i+1)
  {
    y=y*i;
  }
  return y;
}

double mah_d(const arma::mat& x) //TODO potentially add buffer scaling for the ellipsoid Fk (i.e. C--> fk*C)
{
  //Mahalanobis distance: a measure of how far a set of points are from a distribution
  int Ns = x.n_rows;
  int Nd = x.n_cols;

  arma::mat C(Nd,Nd); 
  C = arma::cov(x, 1);  
  arma::vec M = matmean(x);  
  arma::mat Cinv(Nd, Nd);
    Cinv = C.i();  
  arma::vec X_mu(Nd);

  for (int i=0; i<Nd; i++) 
  {
    for (int j=0; j<Ns; j++) 
    {
      X_mu(i) = x(j,i) - M(i);
    }  
  }
 
  arma::vec X_muC(Nd);
     X_muC = Cinv*X_mu; 
  double d; 
    d = dot(X_mu,X_muC);
    
  return d;
}



double V_e(const arma::mat& x)
{
  //Ellipse volume: volume of the fitted ellipse
//it works but im skeptical..TODO we should try this on an ellipse we know the volume of
  int Ns = x.n_rows;
  int Nd = x.n_cols;
  double Vd, val, sign, d, vol, M;

  if(Nd % 2 == 0)
	{ Vd = pow(M_PI,Nd/2)/factorial(Nd/2) ;}
  else
	{ Vd = pow(2, Nd)*pow(M_PI,(Nd-1)/2)*factorial((Nd-1)/2)/factorial(Nd);}
  
  arma::mat C(Nd,Nd);
  C = arma::cov(x, 1);
  //C.print();

  d = det(C);  // or use: log_det(val, sign, C);   //armadillo says it's better to use log_det for big matrices 
  						   //d = sign*exp(val);  
  d = abs(d);
  
  M = mah_d(x);

  vol = Vd*pow(d,1/2)*pow(M,Nd/2);
  return vol;
}

double V_sk(const arma::mat& x, const arma::mat& s, int i)
{
  //sample volume, estimate of the prior volume.  
  //arguments:  i=iteration of nested sampling; s=matrix of sample points(in this ellipsoid); x=matrix of total points.
  
  int N = x.n_cols;
  int n = s.n_cols;
  double vol;
  vol = (n/N)*exp(-i/N); 
  return vol;
}



double hk(const arma::mat& x, const arma::mat& s, int i)
{  //calculates hk for a sample of points in the kth ellipse (matrix s, a subset of the total points x, for the i'th nested sampling iteration)   hk is to be used in finding the best K for K-means sampling
  double h_k;
  h_k = (V_e(s)/V_sk(x,s,i))*mah_d(x);
  return h_k;
}

*/

arma::mat elli_sample(const arma::mat& s)
{
  //Given a matrix of samples, s, (s=x for the first iteration), returns a new sample point from within the ellipse 

  int Ns = s.n_rows;
  int Nd = s.n_cols;  

  arma::mat C(Nd,Nd);
  C = arma::cov(s, 1);
  arma::mat Cinv(Nd,Nd);
  
  arma::vec mu(Nd);
  mu = matmean(s);

  // General form of equation is (x.t()*Covariance.inv()*x<=1) 
  // Calculates a,b,..etc for ((x-xmean)/a)^2+(y-ymean)/b)^2 <= I. in diagonal matrix(1/a,1/b,..), i.e. diagonalizing the inverse covariance matrix. (called coeffs)

  arma::mat coeffs(Nd,Nd);

  coeffs = diagmat(C.i()); 
  
  arma::cx_vec eigval;  // armadillo's cx = complex 
  arma::cx_mat eigvec;
  arma::eig_gen(eigval, eigvec, diagmat(C));
  
  // (Confidence interval options are needed for ellipse size).. I is the confidence interval of the distrubtion made from the sample points: 
  // I = 4.605,5.991,9.210/-->/90,95,99% intervals respectively. (approximated with Chi-squared)

  double I = 9.210;  // Set for 95% confidence
  int n = 1;         // Samples 1 point at a time 

  double t = time(0);		// Merssenne-twister random number with clock time
  std::mt19937 rn(t);
  double max = rn.max();
  double z = rn();

  arma::vec Mx(Nd);
  arma::vec newp(Nd); // New point
  double E; 
  char flag = 'F';

  while(flag != 'T')
  { 
    for(int j=0; j<Nd; j++)
	  { 
	    z = ((rn()/max)-0.5);   
      Mx(j) = sqrt(I*abs(eigval(j)));  // Axes
	    newp(j) = (Mx(j)*z);  // Picking a random number [-ellipseaxis/2, ellipseaxis/2] and moving to the mean 	  
	    newp(j) = newp(j) +mu(j);
    }
    E = dot(newp,coeffs*newp);
    if(E<=I)
    {
      flag = 'T';
    }  
    for(int j=0; j<Nd; j++)
    {
      Mx(j)=0;
    }
  } 
  return newp;
}

void save_final(int ns, const int N, arma::vec& L, arma::mat& x, arma::mat& disc_pts, arma::mat& disc_L, int steps)
{
  /// Function to print out the discarded & final likelihood values to file
  std::string filename;
  std::cout << "Please specify filename for csv output, e.g. output.csv" << std::endl;
  std::cin >> filename;
  std::ofstream output;
  output.open(filename.c_str());
  output << "Likelihood, Coordinates\n"; // Set columns
  for (int i=0; i<steps; i++) 
  {
  output << disc_L(i) << ",";
    for (int j=0; j<N; j++)
    {
      output << disc_pts(i,j) << ",";
    }
  output << "\n";
  }
  for (int i=0; i<ns; i++) 
  {
  output << L(i) << ",";
    for (int j=0; j<N; j++)
    {
      output << x(i,j) << ",";
    } 
  output << "\n";
  }
  output.close();
}

double Nest(double (*likelihood)(const arma::vec& v), arma::vec (*prior)(int), int ns, int steps, const int N, double alpha) 
{
  /// User inputs: "prior" (as a function of dimensionality of problem, "N"), "likelihood" (as a function of a
  /// vector of input points, "v"), the number of samples on the volume, "ns", and the maximum number of steps for
  /// the function until termination, "steps".
  
  /// Initialisation
  arma::mat x(ns, N);             // Points on parameter space
  arma::vec L(ns);                // Likelihood vector
  double Z=0;                     // Evidence
  double Xp=1;                    // Prior volume  
  double weight = exp((1.0/ns-1));// Weight needed for evidence calculation
  
  /// Sample from prior to fill up matrix
  for (int i=0; i<ns; i++)
  {
    x.row(i) = prior(N).t();
  }
  
  int worst_pt_location;          // Location of worst point in x
  double L_min;                   // Minimum likelihood point  
  arma::mat disc_pts(steps,N);    // Store the discarded points
  arma::vec disc_L(steps);        // Store the discarded likelihood values
     
  /// Compute likelihood for the original supplied points
  for (int i=0; i<ns; i++) 
  {
    L(i) = likelihood(x.row(i).t());
  }
  /// Find smallest minimum value and it's location
  L_min = L.min();  
  worst_pt_location = L.index_min();
    
  arma::vec temp(N);              // Temporary vector for new point
  double L_temp;                  // Temporary likelihood
  
  for (int i=0; i<steps; i++) 
  {
    // Add worst point to discard matrices
    disc_pts.row(i) = x.row(worst_pt_location);
    disc_L(i) = L_min;
    
    Xp = exp(-(float)i/ns);       // Update prior volume
    /// Update evidence 
    Z = Z + L_min * Xp * weight;
    
    // Sample from prior with condition L(new) > L_min
    L_temp = 0;
    while (L_temp < L_min) 
    {
    //Currently samples from ellipses, change to temp = prior(N); to sample from prior.
      temp = elli_sample(x);
      L_temp = likelihood(temp);
    }
    
    // Insert new point + likelihood once accepted
    x.row(worst_pt_location) = temp.t();    
    L(worst_pt_location) = L_temp;

    // Find new minimum likelihood & it's location
    L_min = L.min();  
    worst_pt_location = L.index_min();
    
    std::cout << "convergence criteria: is" << L.max()*Xp << " < "<<alpha*Z<<"??" << std::endl; 

    /// Convergence criteria 
    if (L.max()*Xp < alpha*Z) 
    {
      std::cout << "Convergence criteria has been met, at iteration number: " << i << std::endl;
      // Add up the non-discarded points -> final Z
      double sum;
      for (int k=0; k<ns; k++) 
      {
        sum = sum + L(k);
      }      
      Z = Z + sum/ns;
      save_final(ns, N, L, x, disc_pts, disc_L, i);
      return Z;
    }
        
  }
  /// Algorithm has not converged -> hopefully enough steps for a decent final state.
  // Add up the non-discarded points -> final Z
  double sum;
  for (int k=0; k<ns; k++)  
  {
    sum = sum + L(k);
  }      
  Z = Z + sum/ns;
  save_final(ns, N, L, x, disc_pts, disc_L, steps);
  return Z;
}
