#include <iostream>
#include <cmath>
#include <random>
#include <ctime>
#include <armadillo>
#include "ExternNest.hpp"
#include <fstream>

/*

// Likelihood function as 2x2D Gaussians
double likelihood(const arma::vec& v) 
{
  double sigma_x = 5;
  double sigma_y = 5;
  double mu_x = 80;
  double mu_y = 25;
  double sigma_x_2 = 5;
  double sigma_y_2 = 5;
  double mu_x_2 = 20;
  double mu_y_2 = 70;
  return 2*exp(-(v(0)-mu_x)*(v(0)-mu_x)/(2*sigma_x*sigma_x)-(v(1)-mu_y)*(v(1)-mu_y)/(2*sigma_y*sigma_y))/(4*M_PI*sqrt(sigma_x*sigma_y)) + exp(-(v(0)-mu_x_2)*(v(0)-mu_x_2)/(2*sigma_x_2*sigma_x_2)-(v(1)-mu_y_2)*(v(1)-mu_y_2)/(2*sigma_y_2*sigma_y_2))/(4*M_PI*sqrt(sigma_x_2*sigma_y_2));
}

*/

/*

// Liklihood function as Himmelbau   (uniform prior [-5,5])
double likelihood(const arma::vec& v) 
{
  double x = v(0);
  double y = v(1);
  return exp(-((x*x+y-11)*(x*x+y-11) + (x+y*y-7)*(x+y*y-7)));
}

*/

// Liklihood function as Eggbox  (uniform prior [0,10*pi])
double likelihood(const arma::vec& v) 
{
  double x = v(0);
  double y = v(1);
  return exp(-(2+cos(x/2)*cos(y/2))*(2+cos(x/2)*cos(y/2))*(2+cos(x/2)*cos(y/2))*(2+cos(x/2)*cos(y/2))*(2+cos(x/2)*cos(y/2))*(2+cos(x/2)*cos(y/2)));
}

/*

// Liklihood function as Rastrigin   (uniform prior [-5.12,*5.12])
double likelihood(const arma::vec& v) 
{
  double x = v(0);
  double y = v(1);
  return -exp((20+x*x-10*cos(2*M_PI*x)+y*y-10*cos(2*M_PI*y)));
}

*/

arma::vec prior(int N)
{
  // Simple uniform prior on 0-100 in all N dimensions (unless specified with the function)
  arma::vec point(N,arma::fill::randu);
  point = point *10*M_PI;
  return point;
}


int main() 
{
  int steps = 100;            // Maximum number of steps before terminating, gets really slow to run >~1000 
  int ns = 1000;              // Number of samples,  
  const int N = 2;            // 2-D function
  double Z;                   // Evidence
  double alpha = 0.01;        // Convergence criteria 
  
  Z = Nest(likelihood, prior, ns, steps, N, alpha);
  std::cout << "Evidence = " << Z << std::endl;
  
  return 0;
}
