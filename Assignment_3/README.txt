Files contained within this directory are:

-----------
.cpp files
-----------

rng_histo.cpp - creates and bins as many random numbers as the user 
requires.

transsin.cpp - uses the transformation method to plot sin(x)/2 between 
0 and Pi. Must be compiled as g++ -std=c++11 transsin.cpp -o transsin.x
as the function uses the inbuilt c++11 <random> Mersenne Twister.

rejectsin.cpp - uses the rejection method to plot 2*sin^2(x)/Pi between
0 and Pi, with 2*sin(x)/Pi as the comparison function. Must be compiled as g++ -std=c++11 rejectsin.cpp -o rejectsin.x as the function uses the inbuilt c++11 <random> Mersenne Twister.

-----------
.dat files
-----------
These are just example files used for the histograms in the report. The
.png files are also included.
  - rng_histo.dat  -  data for uniform histogram on [0,1]
  - sin_histo.dat  -  data for histogram of sin(x)/2 on [0,Pi]
  - sin2_histo.dat -  data for histogram of 2*sin^2(x)/Pi on [0,Pi]


Otherwise, the Answers.pdf and the .png files are self-explanatory.
