#include <iostream>
#include <cmath>
#include <fstream>
#include <string>
#include <ctime>
#include <random>
using namespace std;

int main() {
  long points = 100000; b        // Number of numbers to generate
  double r[points], t[3*points]; // Arrays of random numbers
  long t = time(0);              // Clock time

  // Using Mersenne Twister: see https://en.wikipedia.org/wiki/Mersenne_Twister
  mt19937 gen(t);       // Generates random number based on t.
  uniform_real_distribution<double> dis(0.0, 1.0); // Specifies range [0,1]
  
  for (int i=0;i<points;i++){
    // Generate 100000 random numbers from 0 to 1 for pdf.
    r[i] = dis(gen);
  }

  for (int i=0;i<3*points;i++){
    // Generate 300000 random numbers from 0 to 1 for c(y).
    t[i] = dis(gen);
  }

  int num_bins = 200;
  int bins[num_bins]={0}, m;
  float binsize = M_PI/num_bins, binrange;

  // Details of file to write out to.
  string filenameout;
  cout << "Specify output filename" << endl;
  cin >> filenameout;
  ofstream output;
  output.open(filenameout.c_str());
  cout << "Calculating and writing to file..." << endl;
  
  for (int i=0;i<points;i++){
    // Saves random number and value of function 
    output << endl;
  }
  
  cout << "Done." << endl;
  return 0;
}
