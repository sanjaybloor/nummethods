#include <iostream>
#include <cmath>
#include <fstream>
#include <string>
#include <ctime>
#include <random>
using namespace std;

double inv(double x) {
  // Inverse function
  double yx = acos(1-2*x);
  return yx;
}

int main() {
  long points = 100000; // Number of numbers to generate
  double r[points];     // Array of random numbers
  long t = time(0);     // Clock time

  // Using Mersenne Twister: see https://en.wikipedia.org/wiki/Mersenne_Twister
  mt19937 gen(t);       // Generates random number based on t.
  uniform_real_distribution<double> dis(0.0, 1.0); // Specifies range [0,1]
  
  for (int i=0;i<points;i++){
    // Generate 100000 random numbers from 0 to 1. 
    r[i] = dis(gen);
  }

  int num_bins = 200;
  int bins[num_bins]={0}, m;
  float binsize = M_PI/num_bins, binrange;

  for (int i=0; i<points; ++i) {
    m = floor(inv(r[i])/binsize);
    ++bins[m];
  }
  
  // Details of file to write out to.
  string filenameout;
  cout << "Specify output filename for binned data:" << endl;
  cin >> filenameout;
  ofstream output;
  output.open(filenameout.c_str());
  cout << "Calculating and writing to file..." << endl;

  for (int i=0; i<num_bins; ++i) {
    binrange = i*binsize;
    output << binrange << " " << bins[i] << endl;
  }

  cout << "Done." << endl;
  return 0;
}

