Files contained within this directory are:

 ------------------
|.cpp and .h files |
 ------------------

trapezium.cpp 
Integrates external function found in function.cpp using the trapezium
rule. It asks the user for start and end points, as well as relative
accuracy. Compile as:

g++ trapezium.cpp trapsolver.cpp function.cpp trap_area.cpp -o trapezium.x

----

simpsons.cpp 
Integrates external function found in function.cpp using Simpson's rule.
It asks the user for start and end points, as well as relative accuracy.
Compile as:

g++ simpsons.cpp trapsolver.cpp function.cpp trap_area.cpp -o simpsons.x

----

function.cpp (function.h) 
Contains external 1-D function f(double x) specified by the user, which
is called by both simpsons.cpp and trapezium.cpp. 

----

trapsolver.cpp (trapsolver.h) 
Algorithm to compute new value of T[n+1] from T[n]. Uses trap_area.cpp.

----

trap_area.cpp (trap_area.h) 
Algorithm to the compute f(x) at new points halfway between previous ones. 
