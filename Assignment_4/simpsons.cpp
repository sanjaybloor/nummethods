#include <iostream>
#include <cmath>
#include <string>
#include "function.h"
#include "trapsolver.h"
using namespace std;

// Program to perform Simpsons rule integration on a user-specified
// function. This requires an external function.cpp file and function.h
// file. The function should be saved as f(x). See the example given. 

int main() {
  double a, b, E; 
  cout << "Simpsons rule estimator, estimating area of f(x) between a and b to an accuracy of E. Please ensure function.cpp and function.h have been changed to the correct functions accordingly." << endl;
  cout << "Please specify the starting value a." << endl;
  cin >> a;
  cout << "Please specify the end value b." << endl;
  cin >> b;
  cout << "Please specify the relative accuracy E<1." << endl;
  cin >> E;
  cout << "Doing my thing..." << endl;  

  double h = b-a;
  double aplusb = a+b;
  double Sold=0, Snew=0, ratiotest=1, Told=0, Tnew =0;
  int n=1;
  Tnew = (f(a)+f(b))*h/2;
  
  // Initial value S_1 = (4T_2-T_1)/3;
  Snew = (4*trapsolver(aplusb, n, h, Tnew)-Tnew)/3;
  cout << "Initial guess is:  S_1 = " << Snew << endl;

  while (ratiotest > E) {
    Sold = Snew;
    Tnew = trapsolver(aplusb, n, h, Tnew);
    Snew = (4*trapsolver(aplusb, n+1, h, Tnew)-Tnew)/3;
    n++;
    cout << "Current estmate of S_" << n << " = " << Snew << endl;
    ratiotest = abs(Snew/Sold - 1);
  }

  cout << "Ratio test value: " << ratiotest << " for iteration n = " << n-1 << endl;
  cout << "Estimation of area A = " << Snew << endl;

  return 0;
}
