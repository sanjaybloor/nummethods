#include <iostream>
#include <cmath>
#include "function.h"
using namespace std;

double trap_area(double aplusb, int n, double h) {
  double Inew=0; 
  double a=pow(2,n-1);
  double coeff=1/(2*a);

  for (int i=0;i<a;i++) {
    Inew = Inew + h*coeff*f(aplusb*(1+2*i)*coeff);
  }

  return Inew;
}
