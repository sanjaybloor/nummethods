#include <iostream>
#include <cmath>
#include "function.h"
#include "trap_area.h"
using namespace std; 

double trapsolver(double aplusb, int n, double h, double I) {
  I = 0.5*I + trap_area(aplusb, n, h);
  return I;
}
