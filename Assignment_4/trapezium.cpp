#include <iostream>
#include <cmath>
#include <string>
#include "function.h"
#include "trapsolver.h"
using namespace std;

// Program to perform trapezium rule integration on a user-specified
// function. This requires an external function.cpp file and function.h
// file. The function should be saved as f(x). See the example given. 

int main() {
  double a, b, E; 
  cout << "Trapezium rule estimator, estimating area of f(x) between a and b to an accuracy of E. Please ensure function.cpp and function.h have been changed to the correct functions accordingly." << endl;
  cout << "Please specify the starting value a." << endl;
  cin >> a;
  cout << "Please specify the end value b." << endl;
  cin >> b;
  cout << "Please specify the relative accuracy E<1." << endl;
  cin >> E;
  cout << "Doing my thing..." << endl;  

  double h = b-a;
  double aplusb = a+b;
  double Told=0, Tnew=0, ratiotest=1;

  Tnew = (f(a)+f(b))*h/2;
  cout << "Initial guess is:  T_1 = " << Tnew << endl;
  int n=1;
  while (ratiotest > E) {
    Told = Tnew;
    Tnew = trapsolver(aplusb, n, h, Tnew);
    ++n;
    cout << "Current estmate of T_" << n << " = " << Tnew << endl;
    ratiotest = abs(Tnew/Told - 1);
  }

  cout << "Ratio test value: " << ratiotest << " for iteration n = " << n-1 << endl;
  cout << "Estimation of area A = " << Tnew << endl;

  return 0;
}
