#include <iostream>
#include <cmath>
using namespace std;

int f(double x) {
  return 2*exp(-x*x)/sqrt(M_PI);
}
