#include <iostream>
using namespace std;

// Machine epsilon is going to be a power of two. Therefore iterate over
// powers of two until the machine can no longer identify that 1.0 + epsilon
// is greater than 1.0. 

int main() {
  cout << "==== FLOAT ====" << endl;
  float epsilonf = 1.0; 
  float totalf = 1.0 + epsilonf;
  int countf = 0; // This counts the power of 2 for epsilon.
  while (totalf > 1.0) {
    epsilonf = epsilonf/2;
    totalf = 1.0 + epsilonf;
    countf = countf - 1;
  }
  cout << "Machine epsilon (float) calculated to be: " << epsilonf << endl;
  cout << "This is 2^(" << countf << ")" << endl;


  cout << "==== DOUBLE ====" << endl;
  double epsilond = 1.0; 
  double totald = 1.0 + epsilond;
  int countd = 0; // This counts the power of 2 for epsilon.
  while (totald > 1.0) {
    epsilond = epsilond/2;
    totald = 1.0 + epsilond;
    countd = countd - 1;
  }
  cout << "Machine epsilon (double) calculated to be: " << epsilond << endl;
  cout << "This is 2^(" << countd << ")" << endl;

  
  cout << "==== LONG DOUBLE ====" << endl;
  long double epsilone = 1.0; 
  long double totale = 1.0 + epsilone;
  int counte = 0; // This counts the power of 2 for epsilon.
  while (totale > 1.0) {
    epsilone = epsilone/2;
    totale = 1.0 + epsilone;
    counte = counte - 1;
  }
  cout << "Machine epsilon (long double) calculated to be: " << epsilone << endl;
  cout << "This is 2^(" << counte << ")" << endl;
  
  return 0;
  

}
