#include <iostream>
#include <cmath>
using namespace std;

int main() {
  signed int i; signed int tempi=100;
  signed long l; signed long templ=100;
  float f; float tempf=100.0;
  double d; double tempd=100.0;
  cout << "Calculating x^10 for various types..." << endl << "Trying long integer..." << endl;
  for (int k = 0; k<10000; ++k) {
    l = pow(k,10);
    if (abs(templ-l) < 1) {
      cout << "Long integer has overflown at at " << k << "^10" << endl;
      break;
    }
    templ = l;
  }
  cout << "Trying float..." << endl;
  for (int k = 0; k<10000; ++k) {
    f = pow(k,10);
    if (abs(tempf-f) < 1 || isinf(f) == 1) {
      cout << "Float has overflown at at " << k << "^10" << endl;
      break;
    }
    tempf = f;
    if (k == 9999) {
      cout << "Final iteration gives (9999)^10 = " << f << endl;
    }
  }
  cout << "Trying double..." << endl;
  for (int k = 0; k<10000; ++k) {
    d = pow(k,10);
    if (abs(tempd-d) < 1 || isinf(d) == 1) {
      cout << "Double has overflown at at " << k << "^10" << endl;
      break;
    }
    tempd = d;
    if (k == 9999) {
      cout << "Final iteration gives (9999)^10 = " << d << endl;
    }
  }
  cout << "Finished." << endl;
  //  for (int i = 0; i<=10000; ++i) {
  //    d = pow(i,10);
  //    cout << d << " double" << endl;
  //  }
  //  for (int j = 0; j<=10000; ++j) {
  //    s = pow(j,10);  
  //  }
  return 0;
}
