#include <iostream>
#include <cmath>
using namespace std;

int main() {
  long l; long templ=100;
  float f; float tempf=100.0;
  double d; double tempd=100.0;
  cout << "Calculating e^x^2 for various types..." << endl << "Trying long integer..." << endl;
  for (int k = 0; k<10; ++k) {
    l = exp(k*k);
    if (abs(templ-l) < 1) {
      cout << "Long integer has overflown at at exp(" << k << "^2)" << endl;
      break;
    }
    templ = l;
  }
  cout << "Trying float..." << endl;
  for (int k = 0; k<10; ++k) {
    f = exp(k*k);
    if (abs(tempf-f) < 1 || isinf(f) == 1) {
      cout << "Float has overflown at at exp(" << k << "^2)" << endl;
      break;
    }
    tempf = f;
    if (k == 9) {
      cout << "Final iteration gives exp(81) = " << f << endl;
    }
  }
  cout << "Trying double..." << endl;
  for (int k = 0; k<10; ++k) {
    d = exp(k*k);
    if (abs(tempd-d) < 1 || isinf(d) == 1) {
      cout << "Double has overflown at at exp(" << k << "^2)" << endl;
      break;
    }
    tempd = d;
    if (k == 9) {
      cout << "Final iteration gives exp(81) = " << d << endl;
    }
  }
  cout << "Finished." << endl;
  return 0;
}
