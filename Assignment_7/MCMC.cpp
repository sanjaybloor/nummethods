#include <iostream>
#include <cmath>
#include <random>
#include <ctime>
#include <string>
#include <fstream>
using namespace std;

double MCMC(int dim, double *x, double *sigma, double(*function)(double*), int burn_in, const int buffer_len, double threshold) { 
  // MCMC using Gaussians and that
  // *x -> solution vector
  // dim -> dimensionality of problem
  // *function(double*) -> f(x) to maximise
  double xtrial[dim];
  double mu[dim];
  double f_current, f_new, alpha, beta;
  double buffer[dim][buffer_len]={0}; // Buffer to compute the rolling variance
  double calc_var[dim]; // Computed variance from previous estimates of position
  double sigma_new[dim];
  double maxstddev;
  int index=1;

  // Writing to output
  string filename;
  cout << "Specify output filename" << endl;
  cin >> filename;
  ofstream output;
  output.open(filename.c_str());
 
  for (int i=0; i<dim; i++) {
    output << "  x[" << i << "]   sigma(x[" << i << "])";
  }
  output << " f(x)    iteration " << endl;

  double t = time(0); // Uses clock time as seed.
  mt19937 gen(t);     // Random number generator. (Mersenne Twister) 
  uniform_real_distribution<double> dis(0, 1); // Specifies range [0,1]

  f_current = function(x); // Starting value of f(x).
  int j=0; // Counter

  while (-1) {
    // Write current values of x to file.
    for (int i=0; i<dim; i++) {
      output << x[i] << "  " << sigma[i] << "  ";
    }
    output << f_current << " " << j << endl;

    // Draw randomly from Gaussian around current x & sigma.
    for (int i=0; i<dim; i++) {
      normal_distribution<double> norm(x[i], sigma[i]);  
      xtrial[i] = norm(gen);
    }
 
    // Compute value of f(xtrial)
    f_new = function(xtrial);

    if (f_new > f_current) {
      alpha = 2; // Set alpha so that it's accepted without fail.      
    }
    else {
      alpha = abs(f_current/f_new); // Probability of accepting. 
    }
    beta = dis(gen);         // Random number 0<b<1
    if (alpha > beta) {
      // Accept if alpha > beta 
      for (int i=0; i<dim; i++) {
        x[i] = xtrial[i];
      }
      f_current = f_new; // Update value of f. 
    }

    // Update rolling buffer
    for (int i=0; i<dim; i++) {
      buffer[i][index] = x[i];
    }
    index = (index+1)%buffer_len;

    for (int i=0; i<dim; i++) {
      for (int k=0; k<buffer_len; k++) {
        mu[i] = mu[i] + buffer[i][k];   
      }
      mu[i] = mu[i]/buffer_len; // Compute mean of buffer
      //cout << mu[i] << endl;
    }
    
    for (int i=0; i<dim; i++) {
      for (int k=0; k<buffer_len; k++) {
        calc_var[i] = calc_var[i] + (buffer[i][k]-mu[i])*(buffer[i][k]-mu[i]);
      }
      calc_var[i] = calc_var[i]/buffer_len; // Compute variance of each distribution
      sigma_new[i] = sqrt(calc_var[i]);
    }
    
   

    if (j > burn_in) { // After burn-in... hopefully 
      // Adapt variance from the buffer
      for (int i=0; i<dim; i++) {
        sigma[i] = sigma_new[i];
      }
      maxstddev = sigma[0];
      for (int i=1; i<dim; i++) {
        if (sigma[i] > maxstddev) {
          maxstddev = sigma[i];
        }
      }      
      if (maxstddev < threshold) {
        if (maxstddev < 1E-10) {
          cout << "I haven't moved in " << buffer_len << " iterations. Total number of iterations = " << j << endl;
          break;
        }
        else {
          cout << "Standard deviation is less than threshold value. Total number of iterations = " << j << endl;
          break;
        }
        maxstddev = threshold+1;
      } 
    } 

    j++; // Increment counter
    for (int i=0; i<dim; i++) { // Reset computation of mean and variance
      mu[i] = 0;
      calc_var[i] = 0;
    }

    if (j>1E6) {
      cout << "1 Million iterations.. That'll do." << endl;
      break;
    }
  }
}




