// External tridiagonal matrix solver
#include <iostream>
using namespace std;

double invert(double *a, double *b, double *c, double *F, double *yPP, int n) {
  // Number of equations n, subdiagonal elements a, diagonal elements b,
  // superdiagonal elements c, solution F. Trying to solve for y''=inv(M)*

  // Forward sweep
  
  c[0] = c[0]/b[0];
  F[0] = F[0]/b[0];

  for (int j=1;j<n-1;j++) {
    c[j]=c[j]/(b[j]-a[j]*c[j-1]);
  }
  for (int j=1;j<n;j++) {
    F[j] = (F[j]-a[j]*F[j-1])/(b[j]-a[j]*c[j-1]);
  }

  // Backwards sweep

  yPP[n-1]=F[n-1];

  for (int j=0;j<n;j++) {
    yPP[n-1-j]=F[n-1-j]-c[n-1-j]*yPP[n-j];
  }
  return 0;
}
