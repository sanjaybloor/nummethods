#include <iostream>
#include <fstream>
#include <string>
#include "trisolver.h"
using namespace std;

int main() {
  string filename;
  int n, i=0; //n is number of lines in file; i is the counter for array
  cout << "Reads in tabulated x-y pairs and performs a cubic interpolation" << endl << "Please specify filename, e.g. Input.dat" << endl;
  cin >> filename;
  cout << "Specify number of lines in file" << endl;
  cin >> n;
  // Read in x-y pairs into array
  double x[n], y[n], t1, t2; // t1 and t2 are temporary inputs for the values
                             // stored in the arrays x and y
  ifstream infile;
  infile.open(filename.c_str());
  
  while (infile >> t1 >> t2) {
    x[i] = t1;
    y[i] = t2;
    ++i;
  }
  infile.close();
  double a[n], b[n], c[n], F[n]={0}; // Master equation coefficients 
  double yPP[n]; // double derivative term to solve

  // Fill in the matrix generically as much as possible without boundary conds
  for (int j = 1; j < n-1; j++) {
    b[j] = (x[j+1] - x[j-1])/3; // b coefficients
    F[j] = (y[j+1]-y[j])/(x[j+1]-x[j]) - (y[j]-y[j-1])/(x[j]-x[j-1]);
  }
  for (int j = 1; j < n+1; j++) {
    a[j] = (x[j] - x[j-1])/6; // a coefficients
  }
  for (int j = 0; j < n-1; j++) {
    c[j] = (x[j+1] - x[j])/6; // c coefficients
 }
  int bcin;
  cout << "Please select boundary conditions: press 1 for natural or 2 to define derivatives." << endl;
  cin >> bcin;
  if (bcin == 1) {
    cout << "Natural boundary conditions chosen: y''=0 at endpoints." << endl;
    // b[0] and b[n-1] are arbitrary.
    // a[n-1], F[n-1], c[0] and F[0] are all zero to force y'' = 0.

    a[n-1] = F[n-1] = c[0] = F[0] = 0;     
    b[0] = b[n-1] = 1;
    
    invert(a, b, c, F, yPP, n);
    cout << endl;
    
    ofstream output;
    output.open("Output_natural.dat");
    // Output x, y, yPP to file.
    for (int j=0;j<n;j++) {
      output << x[j] << "   " << y[j] << "   " << yPP[j] << endl;
    }
    cout << "x, y, y'' saved to Output_natural.dat" << endl;
  }
   
  else if (bcin == 2) {
    double dzero, dn;
    cout << "Enter value of derivative at x[0]." << endl;
    cin >> dzero;
    cout << "Enter value of derivative at x[n-1]." << endl;
    cin >> dzero;

    b[0] = (x[1]-x[0])/3;
    c[0] = (x[1]-x[0])/6;
    a[n-1] = (x[n-2]-x[n-1])/6;
    b[n-1] = (x[n-2]-x[n-1])/3;
    F[0] = (y[1]-y[0])/(x[1]-x[0]) - dzero;
    F[n-1] = (y[n-1]-y[n-2])/(x[n-1]-x[n-2]) - dn;
    
    // Perform matrix inversion
    invert(a, b, c, F, yPP, n);

    ofstream output;
    output.open("Output_specified.dat");
    // Output x, y, yPP to file.
    for (int j=0;j<n;j++) {
      output << x[j] << "   " << y[j] << "   " << yPP[j] << endl;
      }
    cout << "x, y, y'' saved to Output_specified.dat" << endl; 
    }

  
  else {
    cout << "You did not specify 1 or 2. Aborted." << endl;
  }

  // Test for one point
  double xin=1;
  while (xin) {
  cout << "Enter values of x to compute y(x) for. To end, enter a number outside of the input x range." << endl;
  cin >> xin;
  int count=0;
  for (int j=0;j<n;j++){
    if (xin < x[0] || xin > x[n-1]) {
      cout << "x value outside of input range. Terminated." << endl;
      return 0;
    }
    if (xin <x[j]) {
      break;
    }
    ++count;
  }
  cout << "This is between x[" <<count-1 << "] and x[" << count << "]." << endl;
  double yout, A, B, C, D;
  A = (x[count]-xin)/(x[count]-x[count-1]);
  B = 1-A;
  C = (A*A*A-A)*(x[count]-x[count-1])*(x[count]-x[count-1])/6;
  D = (B*B*B-B)*(x[count]-x[count-1])*(x[count]-x[count-1])/6;
  yout = A*y[count-1] + B*y[count] + C*yPP[count-1] + D*yPP[count];
  cout << yout << endl;
  }
  return 0;
}
