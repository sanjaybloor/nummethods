The following files are included:

CubicSpline.cpp -> Reads in (x,y) pairs from input file (Input.dat) - you will
need to specify the number of lines in Input.dat (11). It then lets you choose
boundary conditions - this should all be clear from the terminal. When compiling
you will need to include trisolver.cpp. Also allows the user to compute values
for individual y(x) once the output (x,y,y'') has been saved.

Trisolver.cpp -> External algorithm to compute inverse coefficients for tri-
diagonal matrices.

CubicInt.cpp -> Reads in (x,y,y'') triplets and produces output files to be
plotted (using e.g. gnuplot). Lets the user choose how many points to interpolate.

Input.dat -> The 11-lined table to be read in.
Zero_200.dat -> 200 interpolated points for dy/dx = 0 at the endpoints.
Natural_200.dat -> 200 interpolated points for d2y/dx2 = 0 at the endpoints.
