#include <iostream>
using namespace std;

int main() {
  double x1, y1, x2, y2, x, y;
  cout << "Linear interpolator to find y(x) between two known points (x1,y1) and (x2,y2)" << endl;
  cout << "Enter x1" << endl;
  cin >> x1;
  cout << "Enter y1" << endl;
  cin >> y1;
  cout << "Enter x2" << endl;
  cin >> x2;
  cout << "Enter y2" << endl;
  cin >> y2;
  cout << "Enter x" << endl;
  cin >> x;
  if (x2 > x && x > x1 || x2 < x && x < x1) {
    //Checks that the point is in the correct range
    double A = (x2-x)/(x2-x1);
    double B = 1-A;
    y = A*y1 + B*y2;
    cout << "Point at (" << x << "," << y << ")" << endl;
  }
  else {
    cout << "Point x was not in the range of (x1,x2). Aborted." << endl;
  }
  return 0;
}
