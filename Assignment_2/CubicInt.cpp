#include <iostream>
#include <fstream>
#include <string>
using namespace std;

// This program reads in coefficients given by CubicSpline.cpp. The tabulated
// output should be ordered .dat files of the form x, y, y''.

int main() {

  string filename;
  int n, i=0; //n is number of lines in file; i is the counter for array
  cout << "Reads in tabulated x-y-y'' coefficients and interpolates." << endl << "Please specify filename, e.g. Output.dat" << endl;
  cin >> filename;
  cout << "Specify number of lines in file" << endl;
  cin >> n;
  // Read in x-y-y'' pairs into array
  double x[n], y[n], yPP[n], t1, t2, t3; // t1, t2 and t3 are temporary inputs for the values stored in the arrays x, y and y''
  ifstream infile;
  infile.open(filename.c_str());
  
  while (infile >> t1 >> t2 >> t3) {
    x[i] = t1;
    y[i] = t2;
    yPP[i] = t3;
    ++i;
  }
  infile.close();


  int points;
  cout << "Data successfully entered. How many points would you like to use to interpolate?" << endl;
  cin >> points; // points is now the number of points needed to calculate

  double spacing = (x[n-1]-x[0])/points; // spacing between each successive interpolated points
  double xin[points], yout[points], A, B, C, D;
  int counter=1; // determines which 4 points to use
  double var = x[counter]-x[counter-1]; // define spacing variable here so no recalculation
  for (int j=0;j<points;j++){ // Compute new interpolated values of x and y
    xin[j] = x[0] + j*spacing;
    if (xin[j] > x[counter]) { // Increments counter when past next x value
	++counter;
	var = x[counter]-x[counter-1];
	cout << "Counter increased at iteration number " << j << " and x value " << xin[j] << endl;
      }
    cout << xin[j] << "    " << x[counter] << endl;
    A = (x[counter]-xin[j])/var;
    B = 1-A;
    C = (A*A*A-A)*var*var/6;
    D = (B*B*B-B)*var*var/6;
    cout << A << " " << B << " " << C << " " << D << endl;
    yout[j] = A*y[counter-1] + B*y[counter] + C*yPP[counter-1] + D*yPP[counter];
  }

  string filenameout;
  cout << "Specify output filename" << endl;
  cin >> filenameout;
  ofstream output;
  output.open(filenameout.c_str());
  // Output interpolated values to file.
  for (int j=0;j<points;j++) {
    output << xin[j] << "   " << yout[j] << endl;
  }
  cout << "Interpolated values saved to " << filenameout << endl; 

  return 0;
}
