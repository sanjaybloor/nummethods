#include <iostream>

double RK4(double& x, double y[], double h, double(*f)(double, double*, int), int N){  
  // Performs one RK4 step.

  // x -> dependent variable
  // y[] -> solution vector, y=y(x).
  // h -> step size.
  // N -> number of coupled equations: y=y[N].

  double k1[N], k2[N], k3[N], k4[N], ktemp[N], ktempb[N]; 
  // 2 temp variables needed so as to not use the adjusted values in the same iteration.
  // ... this was a bug for way longer than I care to admit.

  for (int i=0; i<N; i++) {
    k1[i] = h*f(x, y, i);
    ktemp[i] = y[i] + k1[i]/2;
  }
  for (int i=0; i<N; i++) {
    k2[i] = h*f(x+h/2, ktemp, i);
    ktempb[i] = y[i] + k2[i]/2;
  }
  for (int i=0; i<N; i++) {
    k3[i] = h*f(x+h/2, ktempb, i);
    ktemp[i] = y[i] + k3[i];
  }
  for (int i=0; i<N; i++) {
    k4[i] = h*f(x+h, ktemp, i);
  }
  x = x+h;
  for (int i=0; i<N; i++) {
    y[i] = y[i] + (k1[i] + 2*k2[i] + 2*k3[i] + k4[i])/6;
  }
}
