#include <iostream>
#include "RootFinder.h"
#include "RK4.h"
#include <random>
#include <ctime>
#include <string>
#include <fstream>
using namespace std;

/*
  Global variables
                   */

const int N = 2;     // Number of ODEs  
double x_start = 0;  // Start point
double x_end = 10;   // End point 
// Choose step size. Let's not bother with adaptive step sizes, shall we?
int num_steps = 500;
double h = (x_end - x_start)/num_steps;  	
double temp, x, y[N];
double y_end=0;

/*
      Functions
                   */

double function(double x, double y[], int j) {
  if (j == 0) {
    return -y[0]*y[0] - y[1];
  }
  else if (j == 1) {
    return 5*y[0] - y[1];
  }
}

double f(double y2) {
  // Given a y2 -> compute the output of y1
  
  // Uses RK4  
  x = x_start;
  y[0] = 1.5;
  y[1] = y2;

  for (int i=0; i<num_steps;i++) {
    RK4(x, y, h, function, N);
    //cout << x << " | " << y[0] << " | " << y[1] << endl;
  }
   
  return y[0];
}

/*
        Main 
                   */

int main() {

  // Generate random values for y2(x_start). Let's initialise randomly on the interval {-100,100}.
  double t = time(0);
  mt19937 gen(t);      // Random number generator. (Mersenne Twister) 
  uniform_real_distribution<double> dis(-100, 100); 

  double output;

  int upflag = 0, downflag = 0; // Flags to determine when brackets have been successfully found
  double brackets[N];           // Store start values which currently bracket the desired outcome
  double y2, x, temp;

  cout << "Randomising to find values to bracket the ideal solution." << endl;

  // Set boundaries
  y_end = 0;      // y1(10) = 0

  // Find suitable brackets (just randomising)
  while (upflag == 0 || downflag == 0) {

    temp = dis(gen);
    x = x_start;
    y2 = temp;

    // RK4 from a to b
    output = f(y2);

    // Overshot -> save the current value (if better than previous)
    if (output > y_end) {
      if ((upflag == 1 && output > brackets[0]) || (upflag == 0)) {
        brackets[0] = temp;
        upflag = 1; // Set flag
        //cout << "Updated top bracket." << endl;
      } 
    }

    // Undershot -> save value (if better than previous)
    if (output < y_end) {
      if ((downflag == 1 && output < brackets[0]) || (downflag == 0)) {
        brackets[1] = temp;
        downflag = 1; // Set flag
        //cout << "Updated bottom bracket." << endl;
      }     
    }
  }

  cout << "Both brackets are set. Bracketing y2 values are:" << brackets[0] << " and " << brackets[1] << endl << endl;
  cout << "Beginning Brent's method for root finding..." << endl << endl;

  // Use Brent's method now.

  int max_it = 1000;
  double delta = 1E-8;

  double root = brent(f, brackets[0], brackets[1], delta, max_it);
  cout << endl << "Root found for y2 = " << root << endl << endl;

  // Export data
  y[0] = 1.5;
  y[1] = root;
  x = x_start;

  cout << "I'm gonna save this for you now." << endl;
  string filename;
  cout << "Please specify output filename:" << endl;
  cin >> filename;
  ofstream outputfile;
  outputfile.open(filename.c_str());

  outputfile << x << " " << y[0] << " " << y[1] << endl;

  for (int i=0; i<num_steps;i++) {
    RK4(x, y, h, function, N);
    outputfile << x << " " << y[0] << " " << y[1] << endl;
  }

  cout << "Done!" << endl;
  
  return 0;

}

