set key autotitle columnhead

set output "X-Y1.png"
unset key
set term x11 0
set term png
set autoscale
set title 'x-y1 Plane'
plot 'output.dat' using 1:2 with lines
unset output

set output "X-Y2.png"
unset key
set term x11 1
set term png
set autoscale
set title 'x-y2 Plane'
plot 'output.dat' using 1:3 with lines
unset output

set output "Y1-Y2.png"
unset key
set term x11 2
set term png
set autoscale
set title 'y1-y2 Plane'
plot 'output.dat' using 2:3 with lines
unset output
